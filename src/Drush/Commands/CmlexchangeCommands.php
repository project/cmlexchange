<?php

namespace Drupal\cmlexchange\Drush\Commands;

use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drupal\Component\Serialization\Json;

/**
 * A Drush commandfile.
 */
final class CmlexchangeCommands extends DrushCommands {

  /**
   * Ecport 1c connections info. JSON.
   */
  #[CLI\Command(name: 'cmlexchange:info', aliases: ['cmli'])]
  #[CLI\Usage(name: 'cmlexchange:info', description: 'Ecport 1c connections info. JSON.')]
  public function cmlInfo() {
    $config = \Drupal::config('cmlexchange.settings');
    $data = [
      'auth' => $config->get('auth'),
      'user' => $config->get('auth-user'),
      'pass' => $config->get('auth-pass'),
    ];
    $this->output()->writeln(Json::encode($data));
  }

}
