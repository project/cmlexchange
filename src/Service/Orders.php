<?php

namespace Drupal\cmlexchange\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * CommerceML Orders service.
 */
class Orders {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new DebugML object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Query.
   */
  public function xml($from = FALSE) {
    if (!$from) {
      $from = strtotime('now -2 week');
    }
    $orders = [];
    if (\Drupal::moduleHandler()->moduleExists('commerce_order')) {
      $orders = $this->query($from);
      \Drupal::moduleHandler()->alter('cmlexchange_orders_query', $orders);
    }
    // Get Simples XML.
    $xml = '<?xml version="1.0" encoding="UTF-8"?><КоммерческаяИнформация/>';
    if (!empty($orders)) {
      $xml = $this->getXml($orders);
      \Drupal::moduleHandler()->alter('cmlexchange_orders_xml', $xml, $orders);
    }
    // Format XML.
    $dom = new \DOMDocument("1.0");
    $dom->preserveWhiteSpace = FALSE;
    $dom->formatOutput = TRUE;
    $dom->loadXML($xml);
    $dom_xml = $dom->saveXML();
    return $dom_xml;
  }

  /**
   * Query.
   *
   * Use HOOK_cmlexchange_orders_query_alter(&$orders){} for changes.
   */
  public function query($completed = 0, $count = FALSE) {
    $entities = [];
    $entity_type = 'commerce_order';
    $storage = \Drupal::service('entity_type.manager')->getStorage($entity_type);
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('cart', 0)
      ->condition('completed', $completed, '>')
      ->sort('completed', 'DESC');
    if ($count) {
      $query->range(0, $count);
    }
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

  /**
   * Create XML.
   *
   * Use HOOK_cmlexchange_orders_xml_alter(&$xml, $orders){} for changes.
   */
  public function getXml(array $orders) {
    $date = \Drupal::service('date.formatter')->format(time(), 'custom', 'Y-m-d');
    $date_formater = \Drupal::service('date.formatter');
    $order_item_storage = \Drupal::service('entity_type.manager')->getStorage('commerce_order_item');

    $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><КоммерческаяИнформация/>');
    $xml->addAttribute('ВерсияСхемы', '2.09');
    $xml->addAttribute('ДатаФормирования', $date);

    $query = \Drupal::entityQuery('commerce_order')
      // @todo >condition('field_order_unloaded', 0)
      ->accessCheck(FALSE)
      ->condition('cart', 0);
    $result = $query->execute();

    if (!empty($orders)) {
      foreach ($orders as $order_id => $order) {
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        $date = $date_formater->format($order->getCompletedTime(), 'custom', 'Y-m-d');
        $time = $date_formater->format($order->getCompletedTime(), 'custom', 'H:i:s');
        $document = $xml->addChild('Документ');
        $document->addChild('Ид', $order_id);
        $document->addChild('Номер', $order_id);
        $document->addChild('Дата', $date);
        $document->addChild('Время', $time);
        $document->addChild('Валюта', $order->total_price->currency_code);
        $document->addChild('Сумма', $order->total_price->number);
        $document->addChild('ХозОперация', 'Заказ товара');

        $contragents = $document->addChild('Контрагенты');
        $contragent = $contragents->addChild('Контрагент');
        $this->userData($contragent, $order);

        $theGoods = $document->addChild('Товары');
        foreach ($order->getItems() as $key => $orderItem) {
          $id = $orderItem->id();
          $titile = $orderItem->getTitle();
          if ($variation = $orderItem->getPurchasedEntity()) {
            /** @var \Drupal\commerce_product\Entity\ProductVariation $variation */
            $id = $variation->getSku();
            $titile = $variation->getTitle();
            // $goods->addChild('ЦенаЗаЕдиницу', $orderItem->getUnitPrice());
            // $goods->addChild('Сумма', $orderItem->getTotalPrice());
          }
          $goods = $theGoods->addChild('Товар');
          $goods->addChild('Ид', $id);
          $goods->addChild('Наименование', $titile);
          $goods->addChild('ЦенаЗаЕдиницу', $orderItem->unit_price->number);
          $goods->addChild('Количество', $orderItem->getQuantity());
          $goods->addChild('Сумма', $orderItem->total_price->number);
        }
      }
    }
    return $xml->asXML();
  }

  /**
   * Формируем данные из профиля.
   */
  public function userData(&$element, $order) {
    $profile = $order->getBillingProfile();
    $name = $profile->field_customer_fie->value ?? '';
    $phone = $profile->field_customer_phone->value ?? '';
    $city = $profile->field_city->value ?? '';
    $street = $profile->field_street->value ?? '';
    $house = $profile->field_house->value ?? '';
    $apartment = $profile->field_apartment->value ?? '';
    $postcode = $profile->field_postcode->value ?? '';
    $email = $profile->field_customer_email->value ?? '';

    $element->addChild('Наименование', $name);
    $element->addChild('Роль', 'Покупатель');
    $element->addChild('ПолноеНаименование', $name);
    $element->addChild('Телефон', $phone);
    $element->addChild('Город', $city);
    $element->addChild('Улица', $street);
    $element->addChild('Дом', $house);
    $element->addChild('Квартира', $apartment);
    $element->addChild('Индекс', $postcode);
    $element->addChild('ЭлектроннаяПочта', $email);
  }

}
