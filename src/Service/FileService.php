<?php

namespace Drupal\cmlexchange\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * File Service.
 */
class FileService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Debug Service.
   *
   * @var \Drupal\cmlexchange\Service\DebugService
   */
  protected $debugService;

  /**
   * The cml storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $cmlStorage;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The c.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new CheckAuth object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\cmlexchange\Service\DebugService $debug
   *   The debug service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Manager service.
   * @param \Drupal\Core\Entity\FileSystemInterface $file_system
   *   File System service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DebugService $debug,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system
  ) {
    $this->configFactory = $config_factory;
    $this->debugService = $debug;
    $this->cmlStorage = $entity_type_manager->getStorage('cml');
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->fileSystem = $file_system;
  }

  /**
   * Save file (init function).
   */
  public function file($content, $filename, $cid, $type) {
    $this->cml_id = $cid;
    $this->cml = $this->cmlStorage->load($cid);
    $this->type = $type;
    $this->content = $content;
    if (strpos($filename, '.xml')) {
      // 'import.xml', 'offers.xml'.
      $file = $this->saveXml($filename);
    }
    elseif (strpos($filename, '.zip')) {
      $file = $this->saveZip($filename);
    }
    else {
      $file = $this->saveImage($filename);
    }
    return $file;
  }

  /**
   * Save XML.
   */
  private function saveXml($filename) {
    $cml = $this->cml;
    $filepath = $this->cmlDir($cml);
    $file = $this->saveFile($filepath, $filename, FileSystemInterface::EXISTS_RENAME);
    if ($file->id()) {
      $file->display = 1;
      $xmlfiles = $cml->field_file->getValue();
      $xmlfiles[] = ['target_id' => $file->id()];
      $cml->field_file->setValue($xmlfiles);
      $cml->save();
    }
    else {
      $file = FALSE;
    }
    return $file;
  }

  /**
   * Save Image.
   */
  private function saveImage($filename) {
    $path = explode('/', $filename);
    $filename = array_pop($path);
    $filepath = implode('/', $path);
    $file = $this->saveFile($filepath, $filename, FileSystemInterface::EXISTS_REPLACE);
    if (!$file->id()) {
      $file = FALSE;
    }
    return $file;
  }

  /**
   * File scheme from field definition.
   */
  private function getCmlFileScheme() : string {
    $scheme = 'public';
    $def = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('cml');
    if (!empty($def['field_file'])) {
      $scheme = $def['field_file']->getSettings()['uri_scheme'];
    }
    return $scheme;
  }

  /**
   * Save Zip.
   */
  private function saveZip($filename) {
    $cml = $this->cml;
    $filepath = $this->cmlDir($cml);
    $config = $this->configFactory->get('cmlexchange.settings');
    $dir = 'cml-files';
    if ($config->get('file-path')) {
      $dir = $config->get('file-path');
    }
    $scheme = $this->getCmlFileScheme();
    $filepath = "$scheme://{$dir}/{$filepath}/";
    // Fist time save.
    if ($cml->getState() != 'zip') {
      \Drupal::service('file_system')->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY);
      $uri = \Drupal::service('file_system')->saveData($this->content, "{$filepath}{$filename}", FileSystemInterface::EXISTS_REPLACE);
      $file = $this->fileStorage->create([
        'uri' => $uri,
        'uid' => \Drupal::currentUser()->id(),
        'filename' => $filename,
      ]);
      $file->save();
      if ($file->id()) {
        $xmlfiles = $cml->field_file->getValue();
        $xmlfiles[] = ['target_id' => $file->id()];
        $cml->field_file->setValue($xmlfiles);
      }
      $cml->setState('zip');
      $cml->save();
    }
    // APPEND.
    else {
      $path = $this->fileSystem->realpath("{$filepath}{$filename}");
      file_put_contents($path, $this->content, FILE_APPEND);
    }
    return TRUE;
  }

  /**
   * Save File.
   */
  private function saveFile(
    string $path,
    string $filename,
    int $dealing_with_exists_files
  ) {
    $config = $this->configFactory->get('cmlexchange.settings');
    $dir = 'cml-files';
    if ($config->get('file-path')) {
      $dir = $config->get('file-path');
    }
    $scheme = $this->getCmlFileScheme();
    $filepath = "$scheme://{$dir}/";
    $filepath .= !empty($path) ? "{$path}/" : "";
    \Drupal::service('file_system')
      ->prepareDirectory($filepath, FileSystemInterface::CREATE_DIRECTORY);
    $uri = \Drupal::service('file_system')
      ->saveData($this->content, "{$filepath}{$filename}", $dealing_with_exists_files);
    $file = $this->fileStorage->create([
      'uri' => $uri,
      'uid' => \Drupal::currentUser()->id(),
      'filename' => $filename,
      'status' => 1,
    ]);
    $existing_files = $this->fileStorage->loadByProperties(['uri' => $uri]);
    if (count($existing_files)) {
      $existing = reset($existing_files);
      $file->fid = $existing->id();
      $file->setOriginalId($existing->id());
    }
    $file->save();
    return $file;
  }

  /**
   * Get cml_id dir.
   */
  public function cmlDir($cml) {
    $type = $cml->type->value;
    $df = \Drupal::service('date.formatter');
    $time = $df->format($cml->created->value, 'custom', 'Y-m-d--H-i-s');
    $key = substr($cml->uuid->value, 0, 8);
    $cid = $cml->id();
    $dir = "{$type}/{$time}-$key-{$cid}";
    return $dir;
  }

}
