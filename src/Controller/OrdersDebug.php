<?php

namespace Drupal\cmlexchange\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * OrdersDebug.
 */
class OrdersDebug extends ControllerBase {

  /**
   * Page.
   */
  public function page() {
    $from = strtotime('now -2 week');
    $orders = \Drupal::service('cmlexchange.orders');
    $xml = $orders->xml($from);
    $msg = "Заказы с " . \Drupal::service('date.formatter')->format($from, 'custom');
    $this->messenger()->addStatus($msg);
    if (\Drupal::moduleHandler()->moduleExists('devel')) {
      dsm($xml);
    }
    else {
      return ['#markup' => $this->t('Missing module `devel`')];
    }
    return ['#markup' => 'ok'];
  }

}
