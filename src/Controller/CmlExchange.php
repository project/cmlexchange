<?php

namespace Drupal\cmlexchange\Controller;

use Drupal\cmlexchange\Service\DebugService;
use Drupal\cmlexchange\Service\Protocol;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for /cmlexchange Page.
 */
class CmlExchange extends ControllerBase {

  /**
   * Protocol Service.
   *
   * @var \Drupal\cmlexchange\Service\Protocol
   */
  protected $cmlexchangeProtocol;

  /**
   * Debug Service.
   *
   * @var \Drupal\cmlexchange\Service\DebugService
   */
  protected $cmlexchangeDebug;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cmlexchange.protocol'),
      $container->get('cmlexchange.debug')
    );
  }

  /**
   * CmlExchange constructor.
   *
   * @param \Drupal\cmlexchange\Service\Protocol $protocol
   *   Protocol service.
   * @param \Drupal\cmlexchange\Service\DebugService $debug
   *   Debug service.
   */
  public function __construct(
    Protocol $protocol,
    DebugService $debug
  ) {
    $this->cmlexchangeProtocol = $protocol;
    $this->debugService = $debug;
  }

  /**
   * Main.
   */
  public function exchange() {
    // Init.
    $result = $this->cmlexchangeProtocol->init();
    $this->debugService->debug(__CLASS__, "Response: $result");
    // Admin debug.
    if (\Drupal::currentUser()->id() === '1') {
      return ['#markup' => "<pre>{$result}</pre>"];
    }
    // Response.
    $response = new Response($result);
    $response->headers->set('Content-Type', 'text/plain');
    return $response;

  }

  /**
   * Cron .
   */
  public function cron() {
    $cml = \Drupal::service('cmlapi.cml')->query(1);
    $cml = !empty($cml) ? array_shift($cml) : FALSE;
    if (empty($cml)) {
      $text = 'Нет CML для миграции';
    }
    else {
      dsm($cml);
      $text = "CML {$cml->id()} для миграции";
    }
    \Drupal::service('cmlmigrations.migrate')->import();
    return [
      'output' => ['#markup' => $text],
    ];
  }

}
