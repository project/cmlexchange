<?php

namespace Drupal\cmlmigrations\Utility;

use Drupal\Core\Form\FormStateInterface;

/**
 * FormStateInterface with $this->Extra.
 */
interface FormStateExtraInterface extends FormStateInterface {
  //phpcs:disable
  // string $extra
  protected mixed $extra;
  //phpcs:enable

}
