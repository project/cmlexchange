# Обмен данными с 1С
 * Страница отладки выгружаемых товаров: /cmlexchange/orders (Админ -> Конфигурация-> Веб Службы)

```php
<?
/**
 * Implements hook_cmlexchange_orders_query_alter().
 */
function HOOK_cmlexchange_orders_query_alter(&$orders) {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_cmlexchange_orders_xml_alter().
 */
function HOOK_cmlexchange_orders_xml_alter(&$xml) {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

```
